package com.example.pokedex

import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TableRow
import kotlinx.android.synthetic.main.fragment_pokedex.*


class PokedexFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_pokedex, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        for (i in 0 .. table_layout.childCount - 1) {
            val row = table_layout.getChildAt(i) as TableRow
            for (j in 0 .. row.childCount - 1) {
                val btn = row.getVirtualChildAt(j) as ImageButton
                btn.setOnClickListener{
                    pokemonClick(it)
                }
            }
        }
    }

    fun pokemonClick(view: View) {
        val btn = view as ImageButton
        val pokemonName = btn.tag.toString()

        if (resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT) {
            // portrait mode
            val intent = Intent(activity, DetailsActivity::class.java)
            intent.putExtra("pokemon_name", pokemonName)
            startActivity(intent)
        } else {
            // landscape mode
            val detFragment = fragmentManager!!.findFragmentById(R.id.details_fragment) as DetailsFragment
            detFragment.displayPokemon(pokemonName)

        }
    }

}