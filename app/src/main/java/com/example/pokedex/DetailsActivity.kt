package com.example.pokedex

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_details.*
import kotlinx.android.synthetic.main.fragment_details.*

class DetailsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)

        if (intent != null) {
            val pokemonName = intent.getStringExtra("pokemon_name") ?: "Pikachu"
            val imageId = resources.getIdentifier(pokemonName.toLowerCase(), "drawable", packageName)
            val textFileId = resources.getIdentifier(pokemonName.toLowerCase(), "raw", packageName)
            val fileText = resources.openRawResource(textFileId).bufferedReader().readText()
            pokemon_name.text = pokemonName
            pokemon_image.setImageResource(imageId)
            pokemon_details.text = fileText
        }

    }
}