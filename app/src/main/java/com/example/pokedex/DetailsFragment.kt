package com.example.pokedex

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_details.*


class DetailsFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_details, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if (activity?.intent != null) {
            val pokemonName = activity!!.intent.getStringExtra("pokemon_name") ?: "Pikachu"
            displayPokemon(pokemonName)
        }
    }

    fun displayPokemon(pokemonName: String) {
        val imageId = resources.getIdentifier(pokemonName.toLowerCase(), "drawable", activity!!.packageName)
        val textFileId = resources.getIdentifier(pokemonName.toLowerCase(), "raw", activity!!.packageName)
        val fileText = resources.openRawResource(textFileId).bufferedReader().readText()
        pokemon_name.text = pokemonName
        pokemon_image.setImageResource(imageId)
        pokemon_details.text = fileText
    }
}